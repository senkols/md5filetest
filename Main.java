import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Main {
    public static void main(String[] args) throws NoSuchAlgorithmException, IOException {
        //Предварительно проверяем md5-хэш для файла любым генератором
		//напрмиер: https://emn178.github.io/online-tools/md5_checksum.html
		//и заносим в тестовую строку
		String md5HashForReferenceFile = "5cca6069f68fbf739fce37e0963f21e7".toUpperCase();

        //Получаем буферизированный поток - накапливаем данные по файлу в буфер
        BufferedInputStream bufferedInputStream =
                new BufferedInputStream(new URL("https://httpbin.org/image/png").openStream());

        byte[] buffer = new byte[1024];
        int bytes;

        //Получаем инстанс объекта для хеширования
        MessageDigest md5 = MessageDigest.getInstance("MD5");

        //Читаем с буфера файл и хешируем
        while((bytes = bufferedInputStream.read(buffer, 0, buffer.length)) != -1){
            md5.update(buffer, 0, bytes);
        }

        //закидываем с инстанса в отдельный массив полученные байты
        byte[] digest = md5.digest();

        //формируем строку хеша
        StringBuilder sb = new StringBuilder();
        for(byte bite : digest){
            sb.append(String.format("%02x", bite & 0xff));
        }

        //Сравнение хеша удаленного с тестовым, true - файлы идентичны
        System.out.println(sb.toString().toUpperCase().equals(md5HashForReferenceFile));
    }
}
